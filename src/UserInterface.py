import Deck
import Menu

class UserInterface():
    def __init__(self):
        pass


    def run(self):
        """Present the main menu to the user and repeatedly prompt for a valid command"""
        print("Welcome to the Bingo! Deck Generator\n")
        menu = Menu.Menu("Main")
        menu.addOption("C", "Create a new deck")
        
        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "C":
                UserInterface().__createDeck()
            elif command == "X":
                keepGoing = False


    def __createDeck(self):
        """Command to create a new Deck"""
        #User Input for Card Size, Max Number on Cards, and Number of Cards
        repeat = True
        print("To get the deck of cards for thee")
        print("Ye must answer me these questions three")
        while repeat:
            print("#1 What is the size (n) of the card? ([3,15]: nXn)")
            n = int(input());

            if n >= 3 and n <= 15:
                repeat = False

        lowerN = 2*n*n
        upperN = 4*n*n

        repeat = True
        while repeat:
            print(f"#2 What is the max number on the cards? [{lowerN},{upperN}]")
            maxNum = int(input());

            if maxNum >= lowerN and maxNum <= upperN:
                repeat = False

        repeat = True
        while repeat:
            print("#3 How many cards do you wish to have in your deck? [3,10000]")
            cards = int(input());

            if cards >= 3 and cards <= 10000:
                repeat = False

        #Create a new deck
        self.__m_currentDeck = Deck.Deck(n, cards, maxNum)

        #Display a deck menu
        print("You now have a plethora of cards in your deck")
        self.__deckMenu();


    def __deckMenu(self):
        """Present the deck menu to user until a valid selection is chosen"""
        menu = Menu.Menu("Deck")
        menu.addOption("P", "Print a card to the screen");
        menu.addOption("D", "Display the whole deck to the screen");
        menu.addOption("S", "Save the whole deck to a file");

        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "P":
                self.__printCard()
            elif command == "D":
                print()
                self.__m_currentDeck.print()
            elif command == "S":
                self.__saveDeck()
            elif command == "X":
                keepGoing = False


    def __printCard(self):
        """Command to print a single card"""
        cardToPrint = self.__getNumberInput("Id of card to print", 1, self.__m_currentDeck.getCardCount())
        if cardToPrint > 0:
            print()
            self.__m_currentDeck.print(idx=cardToPrint)


    def __saveDeck(self):
        """Command to save a deck to a file"""
        fileName = self.__getStringInput("Enter output file name")
        if fileName != "":
            outputStream = open(fileName, 'w')
            self.__m_currentDeck.print(outputStream)
            outputStream.close()
            print("Done!")

    def __getNumberInput(self,question, numOfCards, max):
        while numOfCards < 2:
            print(question + ":")
            cardToPrint = int(input())

            if cardToPrint <= max and cardToPrint >= 1:
                numOfCards += 1

        return cardToPrint

    def __getStringInput(self, question):
        print(question)

        filename = input()

        while filename == "":
            print(question)
            filename = input()

        return filename
