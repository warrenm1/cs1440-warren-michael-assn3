import sys

import NumberSet

from beautifultable import BeautifulTable

class Card():
    def __init__(self, idnum, size, numberSet):
        """Card constructor"""
        self.__m_idnum = idnum
        self.__m_size = size
        self.__m_cardValues = [] #list of values on the card
        self.__m_numbSet = numberSet
        self.__m_cardValues.append(self.__setupCard(size))


    def __setupCard(self, size):
        """Returns a list: the values found on the card"""
        tempCard = []
        i = 0
        if size % 2 == 0:
            odd = False
        else:
            odd = True
        while i < size:
            temp = self.__m_numbSet.getNext()

            if temp == None:
                temp = self.__m_numbSet.getNext()

            if temp not in tempCard:
                tempCard.append(temp)
                i += 1

        if odd:
            tempCard[int(float(size)/2)] = "FREE!"

        return tempCard

    def getId(self):
        """Return an integer: the ID number of the card"""
        return self.__m_idnum

    def getSize(self):
        """Return an integer: the size of one dimension of the card.
        A 3x3 card will return 3, a 5x5 card will return 5, etc.
        """
        return self.__m_size

    def print(self, file=sys.stdout):
        """void function:
        Prints a card to the screen or to an open file object"""
        ##TODO: Print BeautifulTable
        table = BeautifulTable()

        #table.column_headers = ["This","is","a","test"]
        #table.append_row([1,2,3,4])
        #table.append_row([5,6,7,8])
        #table.append_row([9,10,11,12])
        temp = self.__m_cardValues[:self.__m_size]
        table.column_headers = [str(temp)]

        i = 1
        j = self.__m_size
        while i < (self.__m_size - 1):
            temp = self.__m_cardValues[j:j+self.__m_size]
            table.append_row(temp)
            i += 1
            j += self.__m_size

        temp = self.__m_cardValues[j:]
        table.append_row(temp)


        print(f"Card #{self.getId()}", file=file)
        print(table,file=file)


















        print()
