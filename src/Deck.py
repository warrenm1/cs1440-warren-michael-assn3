import sys

import Card
import NumberSet

class Deck():
    def __init__(self, cardSize, cardCount, numberMax):
        """Deck constructor"""
        self.__m_cardSize = cardSize
        self.__m_cardCount = cardCount
        self.__m_numberMax = numberMax
        self.__m_cards = [] #list of lists...cards in list form
        self.__m_cardId = [] #Unique Identifier
        for i in range(1, self.__m_numberMax + 1):
            self.__m_cardId.append(i)

        self.__m_numSet = NumberSet.NumberSet(len(self.__m_cardId))
        for i in self.__m_cardId:
            self.__m_cards.append(Card.Card(i, self.__m_cardSize, self.__m_numSet))

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Deck deconstructor"""
        self.__m_cardSize.cleanup()
        self.__m_cardCount.cleanup()
        self.__m_numberMax.cleanup()
        self.__m_cards.cleanup()
        self.__m_cardId.cleanup()
        self.__m_numSet.cleanup()


    def getCardCount(self):
        """Return an integer: the number of cards in this deck"""
        return self.__m_cardCount



    def getCard(self, n):
        """Return card N from the deck"""
        card = None
        n -= 1
        if 0 <= n < self.getCardCount():
            card = self.__m_cards[n]
        return card


    def print(self, file=sys.stdout, idx=None):
        """void function: Print cards from the Deck

        If an index is given, print only that card.
        Otherwise, print each card in the Deck
        """
        if idx is None:
            for idx in range(1, self.__m_cardCount + 1):
                c = self.getCard(idx)
                c.print(file)
            print('', file=file)
        else:
            self.getCard(idx).print(file)













