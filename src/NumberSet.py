import random


class NumberSet():
    def __init__(self, size):
        """NumberSet constructor"""
        self.__m_size = size
        self.__m_numSetCount = 0
        self.__m_numSet = []
        for i in range(1, (size + 1), 1):
            self.__m_numSet.append(i)


    def getSize(self):
        """Return an integer: the size of the NumberSet"""
        return self.__m_size

    def get(self, index):
        """Return an integer: get the number from this NumberSet at an index"""
        if index > len(self.__m_numSet) or len(self.__m_numSet) == 0:
            return None
        return self.__m_numSet[index]

    def randomize(self):
        """void function: Shuffle this NumberSet"""
        return random.shuffle(self.__m_numSet)

    def getNext(self):
        """Return an integer: when called repeatedly return successive values
        from the NumberSet until the end is reached, at which time 'None' is returned"""
        if self.__m_numSetCount == len(self.__m_numSet):
            self.__m_numSetCount = 0
            return None

        self.__m_numSetCount += 1
        return self.__m_numSet[self.__m_numSetCount - 1]





